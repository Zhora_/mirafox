<?php
/**
 * Created by Zhora.
 * User: User
 * Date: 2/13/2019
 */
require_once(__DIR__.'../../data.php');

function getTeamInfo($c) {
    global $data;

    $team = $data[$c];

    $teamInfo = [
        'name' => trim($team['name']),
        'avg_scored' => round($team['goals']['scored'] / $team['games']),
        'avg_skiped' => round($team['goals']['skiped'] / $team['games']),
        'success' => round($team['win'] / ($team['draw'] + $team['defeat']), 1),
        'fail' => round($team['defeat'] / ($team['draw'] + $team['win']), 1),
    ];

    return $teamInfo;
}

function match($c1, $c2) {
    $team_c1 = getTeamInfo($c1);
    $team_c2 = getTeamInfo($c2);

    
    $team_c1_to_c2 = ($team_c1['success'] - $team_c2['success']) - ($team_c1['fail'] - $team_c2['fail']);

    if ($team_c1_to_c2 < 1) {
        $team_c1_to_c2 = 1;
    }
    $team_c2_to_c1 = ($team_c2['success'] - $team_c1['success']) - ($team_c2['fail'] - $team_c1['fail']);
    if ($team_c2_to_c1 < 1) {
        $team_c2_to_c1 = 1;
    }

    
    $goal1_to_2 = $team_c1['avg_scored'] * $team_c1_to_c2 * $team_c1['fail'] / $team_c2['success'];

    
    if ($team_c1['success'] - $team_c1['fail'] > $team_c2['success']) {
        $goal1_to_2 = ceil($goal1_to_2);
    } else {
        $goal1_to_2 = floor($goal1_to_2);
    }

    
    $goal2_to_1 = $team_c2['avg_scored'] * $team_c2_to_c1 * $team_c2['fail'] / $team_c1['success'];

    if ($team_c2['success'] - $team_c2['fail'] > $team_c1['success']) {
        $goal2_to_1 = ceil($goal2_to_1);
    } else {
        $goal2_to_1 = floor($goal2_to_1);
    }

    return [$goal1_to_2, $goal2_to_1];
}