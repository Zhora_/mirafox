<?php
/**
 * Created by Zhora.
 * User: User
 * Date: 2/13/2019
 */
$clubInfo = [];
require_once(__DIR__.'/helpers/calculate.php');
?>



<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title>World Cup score</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	</head>

	<body>
    <?php foreach ($data as $i => $c1) {
        foreach ($data as $j => $c2) {
            if ($i == $j) {
                continue;
            }

            $result = match($i, $j);


            $win = $result[0] > $result[1];
            $fail = $result[0] < $result[1];
            $def = $result[0] == $result[1];

            $clubInfo[$c1['name']][] = [
                'guest_team_name' => $c2['name'],
                'result' => $result,
                'home_score' => (int) $result[0],
                'guest_score' => (int) $result[1],
                'win' => $result[0] > $result[1],
                'fail' => $result[0] < $result[1],
                'draw' => $result[0] == $result[1]
            ];
        }
    }

    ?>

<div class="container">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <?php $i=0; foreach ($clubInfo as $team => $data): ?>
        <li class="nav-item">
            <a class="nav-link <?= $i === 0 ? 'active' : '' ?>" id="<?= 'team-'. $i ?>-tab" data-toggle="tab" href="#<?= 'team-'. $i ?>" role="tab" aria-controls="<?= 'team-'. $i ?>" aria-selected="true"><?= $team ?></a>
        </li>
        <?php  $i++; endforeach;?>
    </ul>
    <div class="tab-content" id="myTabContent">
        <?php $i=0; foreach ($clubInfo as $name => $club): ?>
        <div class="tab-pane fade show <?= $i === 0 ? 'active' : '' ?>" id="<?= 'team-'. $i ?>" role="tabpanel" aria-labelledby="<?= 'team-'. $i ?>-tab">

                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th width="33%" scope="col">Home</th>
                        <th width="33%" scope="col">Result</th>
                        <th width="33%" scope="col">Guest</th>
                        <th width="33%" scope="col">*</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($club as $c):?>
                        <?php $className = 'bg-success'?>

                        <?php if ($c['win']) {
                            $className = 'bg-success';
                        } elseif ($c['fail']) {
                            $className = 'bg-danger';
                        } else {
                            $className = 'bg-warning';
                        }?>
                        <tr>
                            <td> <?= $name ?></td>
                            <td><?= $c['home_score']?> - <?= $c['guest_score']?></td>
                            <td><?= $c['guest_team_name']?></td>
                            <td><div style="width: 20px;height: 20px; border-radius: 50%" class="<?= $className?>"></div></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>

        </div>
        <?php $i++; endforeach; ?>
    </div>
</div>

</body>
</html>
